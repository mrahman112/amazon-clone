const testData = [{
    id:666,
    title:"The new Halo wellness band",
    price:29.99,
    image:"https://images-na.ssl-images-amazon.com/images/G/01/kindle/journeys/YjJkZWQ0NTgt/YjJkZWQ0NTgt-MjhiNzdlMmIt-w758._SY608_CB406527709_.jpg",
    rating:5
},
{
    id:777,
    title:"Go wild with trendy prints",
    price:49.99,
    image:"https://images-na.ssl-images-amazon.com/images/G/01/PLF/LARKRO/2020/SPRING-DRIVERS/LR-PRINT-TOP_DT_CC758x608-2X._SY608_CB410269718_.jpg",
    rating:4
},
{
    id:888,
    title:"Top class BP machine",
    price:29.99,
    image:"https://images-na.ssl-images-amazon.com/images/G/01/US-hq/2020/img/Health_x26_Personal_Care/XCM_Manual_1239353_1269289_US_us_health_personal_care_consumables_desktop_cat_card_hpc_1237578_2_3240978_758x608_2X_en_US._SY608_CB408425348_.jpg",
    rating:5
},
{
    id:999,
    title:"Overstock shoes",
    price:49.99,
    image:"https://images-na.ssl-images-amazon.com/images/G/01/AmazonServices/Site/US/Product/FBA/Outlet/Merchandising/Outlet_GW_SH_758x608_B07Z4CP5K1_920fe539._SY608_CB406458921_.jpg",
    rating:4
},
{
    id:123,
    title:"Dell Optiplex 9010 SFF Desktop PC - Intel Core i5-3470 3.2GHz 16GB RAM 240GB SSD DVD Windows 10 Pro, WIFI (Renewed)",
    price:29.99,
    image:"https://m.media-amazon.com/images/I/41zexxGgIpL._AC_SL520_.jpg",
    rating:5
},
{
    id:234,
    title:"Upgrade to Fire TV Stick 4K",
    price:49.99,
    image:"https://images-na.ssl-images-amazon.com/images/G/01/kindle/journeys/YTE3N2JlNjAt/YTE3N2JlNjAt-ZTViYzBkODUt-w758._SY608_CB406792093_.jpg",
    rating:4
}

]

export default testData;