import React from "react";
import "./BasketItems.css";
import { useStateValue } from "./StateProvider";
import FlipMove from "react-flip-move";

function BasketItems({id, image, title, price, rating, hideButton}){

    const [{ basket, singleItem }, dispatch] = useStateValue();

   const removeFromBasket = () => {
       dispatch({
           type: 'REMOVE_FROM_BASKET',
            id: id
       })
   }

    return (
        <div className="checkoutProduct">
            <img className="checkoutProduct__image" src={image} alt=""/>
            <div className="checkoutProduct__info">
                <p className="checkoutProduct__title">{title}</p>
                <p className="checkoutProduct__price">
                    <small>$</small>
                    <strong>{price}</strong>
                </p>
                <div className="checkoutProduct__rating">
                    {Array(rating)
                    .fill()
                    .map(()=> (
                        <p>⭐</p>
                    ))}
                </div>
                <FlipMove> 
                {!hideButton && 
                (<button className="removeItemButton" onClick={removeFromBasket}>Remove from basket</button>
                )}
                </FlipMove>
            </div>
            
           </div>
    );
}

export default BasketItems;