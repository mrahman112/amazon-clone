import React from "react";
import "./Home.css";
import Product from "./Product";
import {Link} from "react-router-dom";

function Home() {

    return (
      <div className="home">
         <div className="home__container">
             <img className="home__image" src="https://images-eu.ssl-images-amazon.com/images/G/02/digital/video/merch2016/Hero/Covid19/Generic/GWBleedingHero_ENG_COVIDUPDATE__XSite_1500x600_PV_en-GB._CB428684220_.jpg?fbclid=IwAR2JD9BeCTj-OYzjgsvadzsmWD-nUFRixHRn97CFWHwmWaiE_rjvF1D3elo"
             alt="" />

<div className="home__row">
         
                <Product 
                    id="666"
                    title="The new Halo wellness band"
                    price={29.99}
                    image="https://images-na.ssl-images-amazon.com/images/G/01/kindle/journeys/YjJkZWQ0NTgt/YjJkZWQ0NTgt-MjhiNzdlMmIt-w758._SY608_CB406527709_.jpg"
                    rating={5}
                />
                
                
                <Product 
                    id="777"
                    title="Go wild with trendy prints"
                    price={49.99}
                    image="https://images-na.ssl-images-amazon.com/images/G/01/PLF/LARKRO/2020/SPRING-DRIVERS/LR-PRINT-TOP_DT_CC758x608-2X._SY608_CB410269718_.jpg"
                    rating={4}
                />

                <Product 
                    id="888"
                    title="Top class BP machine"
                    price={29.99}
                    image="https://images-na.ssl-images-amazon.com/images/G/01/US-hq/2020/img/Health_x26_Personal_Care/XCM_Manual_1239353_1269289_US_us_health_personal_care_consumables_desktop_cat_card_hpc_1237578_2_3240978_758x608_2X_en_US._SY608_CB408425348_.jpg"
                    rating={5}
                />
                
                <Product 
                    id="999"
                    title="Overstock shoes"
                    price={49.99}
                    image="https://images-na.ssl-images-amazon.com/images/G/01/AmazonServices/Site/US/Product/FBA/Outlet/Merchandising/Outlet_GW_SH_758x608_B07Z4CP5K1_920fe539._SY608_CB406458921_.jpg"
                    rating={4}
                />
                
            </div>
             
            <div className="home__row">
                <Product 
                    id="123"
                    title="Dell Optiplex 9010 SFF Desktop PC - Intel Core i5-3470 3.2GHz 16GB RAM 240GB SSD DVD Windows 10 Pro, WIFI (Renewed)"
                    price={29.99}
                    image="https://m.media-amazon.com/images/I/41zexxGgIpL._AC_SL520_.jpg"
                    rating={5}
                />
                
                <Product 
                    id="234"
                    title="Upgrade to Fire TV Stick 4K"
                    price={49.99}
                    image="https://images-na.ssl-images-amazon.com/images/G/01/kindle/journeys/YTE3N2JlNjAt/YTE3N2JlNjAt-ZTViYzBkODUt-w758._SY608_CB406792093_.jpg"
                    rating={4}
                />
                
            </div>
            <div className="home__row">
            <Product 
                    id="345"
                    title="Explore handcrafted goods"
                    price={19.99}
                    image="https://images-na.ssl-images-amazon.com/images/G/01/img18/home/journeys/OTJmY2FmYTAt/OTJmY2FmYTAt-ZGUyZWFmODIt-w758._SY608_CB408650445_.jpg"
                    rating={3}
                />
                <Product 
                    id="456"
                    title="Prepare for any emergency"
                    price={29.99}
                    image="https://images-na.ssl-images-amazon.com/images/G/01/THILGMA/EmergencyPrep/XCM_CUTTLE_1255114_1316000_US_3296347_758x608_2X_en_US._SY608_CB405950253_.jpg"
                    rating={4}
                />
                <Product 
                    id="567"
                    title="Audible Best Sellers"
                    price={9.99}
                    image="https://images-na.ssl-images-amazon.com/images/I/81p2Lfr-z2L._AC_SY220_.jpg"
                    rating={4}
                />
                
            </div>

            <div className="home__row">
                 <Product 
                    id="678"
                    title="Samsung CJ890 Series 49 inch 3840x1080 Super Ultra-Wide Desktop Monitor for Business, 144 Hz, USB-C, HDMI, DisplayPort, 3-Year Warranty (C49J890DKN)

$999.99+ $29.99"
                    price={999.99}
                    image="https://images-na.ssl-images-amazon.com/images/I/81B0xCF%2BPWL._AC_SX679_.jpg"
                    rating={4}
                 />
            </div>
         
         </div>
      </div>);
}

export default Home;