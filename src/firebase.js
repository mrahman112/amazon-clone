import firebase from "firebase";

const firebaseConfig = {
    apiKey: "AIzaSyDFDcb2U_1j-cwkyvG6JEFu3t5j1ZZzDY0",
    authDomain: "miz--clone.firebaseapp.com",
    databaseURL: "https://miz--clone.firebaseio.com",
    projectId: "miz--clone",
    storageBucket: "miz--clone.appspot.com",
    messagingSenderId: "787130511298",
    appId: "1:787130511298:web:ed8edb928806492ee4b05b"
  };

  const firebaseApp = firebase.initializeApp(firebaseConfig);

  const db = firebaseApp.firestore();
  const auth = firebase.auth();

  export { db, auth };

