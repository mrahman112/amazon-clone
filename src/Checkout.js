import React from "react";
import "./Checkout.css";
import Subtotal from "./Subtotal";
import BasketItems from "./BasketItems";
import { useStateValue } from "./StateProvider";

function Checkout() {
    const [{ basket, singleItem, user }, dispatch] = useStateValue();

    console.log("basket ", basket);
    
    return <div className="checkout">
               <div className="checkout__left">
                   <img className="checkout__ad"
                     src="https://images-na.ssl-images-amazon.com/images/G/02/UK_CCMP/TM/OCC_Amazon1._CB423492668_.jpg"
                     alt="" />

                     <div>
                         <h4 className="checkout__userid">{user ? `Hello, ${user.email}` : `Please Sign in to Checkout`}</h4>
                         <h2 className="checkout__title">{basket.length > 0 ? 'Your shopping Basket' : 'Your Amazon Cart is empty'}</h2>
                         {basket.map(item => (
                            <BasketItems 
                                id={item.id}
                                title={item.title}
                                image={item.image}
                                price={item.price}
                                rating={item.rating}

                            />
                         ))}
                         
                         {/* Basket item */}
                     </div>
               </div>
               <div className="checkout__right">
                    <Subtotal />
               </div>
          </div>;
}

export default Checkout;