import React from "react";
import { Link } from  "react-router-dom";

function Sidebar() {

    const closeSidebar = () => {
        document.querySelector(".sidebar").classList.remove("open");
    }

    return (<aside class="sidebar">
            <h3>Shopping Categories</h3>
            <button className="close-sidebar" onClick={closeSidebar}>x</button>
            <ul>
            <Link to="/">
                <li>Electronics</li>
                <li>Home Decorations</li>
            </Link>
            
            </ul>
        </aside>);
}

export default Sidebar;