import axios from "axios";

const instance = axios.create({
    // baseURL: 'http://localhost:5001/miz--clone/us-central1/api' //the api (cloud function ) url
    baseURL: 'https://us-central1-miz--clone.cloudfunctions.net/api'
})

export default instance;