import React, { useEffect } from 'react';
import './App.css';
import { BrowserRouter as Router, Switch, Route, Link} from "react-router-dom";
import Header from './Header';
import Home from './Home';
import Checkout from './Checkout';
import Login from './Login';
import { auth } from './firebase';
import { useStateValue } from "./StateProvider";
import Payment from './Payment';
import { loadStripe } from '@stripe/stripe-js';
import { Elements } from '@stripe/react-stripe-js';
import Orders from './Orders';
import ProductDetails from './ProductDetails';
import Product from './Product';
import Sidebar from './Sidebar';


const promise = loadStripe("pk_test_51HQ0NOHx74djINtB35KY1j7eOJcpHpNqMWzUHcqjX878a2jFm3SwWONgO8QWNYAr8CEjxQRcSgxmPZQlIxRvNyJs00oBFjlSCi");

function App() {

  const [{}, dispatch] = useStateValue();

  useEffect(() => {

    auth.onAuthStateChanged(authUser => {
      if(authUser){
         dispatch({
           type: 'SET_USER',
           user: authUser
         })
      }else{
         dispatch({
           type: 'SET_USER',
           user: null
         })
      }
    })
    
  }, [])

  

  return (
    <Router>
      <div className="app">
          <Switch>
          <Route  path="/productdetails/:id">
                <Header />
                <Sidebar />
                <ProductDetails />
            </Route>
            

          <Route path="/orders">
                <Header />
                <Sidebar />
                <Orders />
            </Route>
          <Route path="/login">
                <Login />
            </Route>
            <Route path="/checkout">
                
                <Header />
                <Sidebar />
                <Checkout />
            </Route>

            <Route path="/payment">
                <Header />
                <Sidebar />
                
                <Elements stripe={promise}>
                   <Payment />
                </Elements>
            </Route>

            <Route path="/">
                
                <Header />
                <Sidebar />
                
                <Home />
            </Route>
              
          </Switch>
      
      </div>
    </Router>
    );
}

export default App;
