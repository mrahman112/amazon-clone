import React from "react";
import "./Product.css";
import { useStateValue } from "./StateProvider";
import ProductDetails from "./ProductDetails";
import { Link, Route } from "react-router-dom";

function Product({ id, title, price, image, rating}) {

    const [{ basket, singleItem }, dispatch] = useStateValue();
   
    const addToDetail = () => {
        //dispatch some action into
        dispatch({
            type: 'ADD_TO_PRODUCTDETAIL',
            item: {
                id: id,
                title: title,
                image: image,
                price: price,
                rating: rating,
            }
        });
    };

    

    return (

    
    <div className="product">
            
             <div className="product__info">
             <Link className="product__info__link" to={`/productdetails/${id}`}>
                 <p onClick={addToDetail}>{title}</p>
                 </Link>
                 <p className="product__price">
                   <small>$</small>
                   <strong>{price}</strong>
                   </p>
                   <div className="product__rating">
                      {Array(rating).fill().map((_, i) => (<p>⭐</p>))}
                       
                   </div>
                   
             </div>
             <Link to={`/productdetails/${id}`}>
             <img onClick={addToDetail} className="product__image" src={image}
                   alt="" />
            </Link>
             {/* <Link className="product__image" to={`/productdetails/${id}`}>
             <img src={image}
                   alt="" />
            </Link> */}
             
             
             {/* <button onClick={addToBasket}>Add to basket</button> */}
             
           </div>);
}

export default Product;