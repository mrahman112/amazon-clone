import React from "react";
import "./ProductDetails.css";
import { useStateValue } from "./StateProvider";
import testData from "./testData";
import Product from "./Product";
import { Link } from 'react-router-dom';

function ProductDetails({id, title, price, image, rating}) {

    

    const [{ basket, singleItem }, dispatch] = useStateValue();

    console.log("prod id ", singleItem[0].title);
   
    const addToBasket = () => {
        //dispatch some action into
        dispatch({
            type: 'ADD_TO_BASKET',
            item: {
                id: singleItem[0].id,
                title: singleItem[0].title,
                image: singleItem[0].image,
                price: singleItem[0].price,
                rating: singleItem[0].rating,
             }
        });
    };

    return (<div>
             <div className="back-to-result">
                 <Link style={{ textDecoration: 'none', color: '#111'}} to="/">Back to result</Link>
             </div>

             <div className="details">
                <div className="details-image">
                    <img  src={singleItem[0].image} alt="" />
                </div>
                 
             

             <div className="details-info">
                 <ul>
                     <li>
                         <h4>{singleItem[0].title}</h4>
                     </li>
                     <li className="product__rating">{Array(singleItem[0].rating).fill().map((_, i) => (<p>⭐</p>))}</li>
                     <li><b>{singleItem[0].price}</b></li>
                 </ul>
             </div>
             <div className="details-action">
                 <ul>
                     <li>
                         Price: {singleItem[0].price}
                     </li>
                     <li>
                         Qty: <select>
                             <option>1</option>
                             <option>2</option>
                             <option>3</option>
                             <option>4</option>
                         </select>
                     </li>
                     <li>
                       <Link to={"/checkout"}>
                        <button  onClick={addToBasket}>Add to basket</button>
                       </Link>
                     </li>
                 </ul>
             </div>

             </div>
             {/* <Product id={singleItem[0].id} title={singleItem[0].title} price={singleItem[0].price}
                 rating={singleItem[0].rating} image={singleItem[0].image} */}
                 
             {/* /> */}

             {/* <div className="product__info">
                 <p>{title}</p>
                 <p className="product__price">
                   <small>$</small>
                   <strong>{price}</strong>
                   </p>
                   <div className="product__rating">
                      {Array(rating).fill().map((_, i) => (<p>⭐</p>))}
                       
                   </div>
             </div>
             <img src={image}
                   alt="" /> */}
             

             </div>)
       
           
}

export default ProductDetails;